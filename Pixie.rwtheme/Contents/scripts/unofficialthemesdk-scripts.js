// 'pixie' is the global object for pixie theme functions
var pixie = {};
// Reduce potential conflicts with other scripts on the page
pixie.jQuery = jQuery.noConflict(true);
var $pixie = pixie.jQuery;
// Create a unique object and namespace for theme functions
pixie.themeFunctions = {};
// Define a closure
pixie.themeFunctions = (function() {
    // When jQuery is used it will be available as $ and jQuery but only inside the closure
    var jQuery = pixie.jQuery;
    var $ = jQuery;
	var $pixie = jQuery.noConflict();

// ExtraContent functionality 
$pixie(document).ready(function () {
	var extraContent =  (function() {
		var ecValue = 10;
		for (i=1;i<=ecValue;i++)
		{
			$pixie('#myExtraContent'+i+' script').remove();
			$pixie('#myExtraContent'+i).appendTo('#extraContainer'+i);
		}
	})();
});

// Function to fade-in the page when the window has finished loading.
// Adjust the delay and fade-in speeds accordingly. Speeds are specified in milliseconds.		
$pixie(window).load(function(){
	$pixie('#pageWrapper').delay(500).fadeIn(2000);
});

// FreeStyle banners
$pixie(document).ready(function(){
	$pixie('#fs').appendTo('#freeStyle');                                           
});

	
})(pixie.themeFunctions);
