window.onload = function()
			{
			settings = {
			tl: { radius: 15 },
			tr: { radius: 15 },
			bl: { radius: 0 },
			br: { radius: 0 },
			antiAlias: true,
			autoPad: true
			}
			settings2 = {
			tl: { radius: 0 },
			tr: { radius: 0 },
			bl: { radius: 15 },
			br: { radius: 15 },
			antiAlias: true,
			autoPad: false
			}
			settings3 = {
			tl: { radius: 15 },
			tr: { radius: 15 },
			bl: { radius: 15 },
			br: { radius: 15 },
			antiAlias: true,
			autoPad: false
			}
			settings4 = {
			tl: { radius: 15 },
			tr: { radius: 15 },
			bl: { radius: 0 },
			br: { radius: 0 },
			antiAlias: true,
			autoPad: true
			}
			settings5 = {
			tl: { radius: 0 },
			tr: { radius: 0 },
			bl: { radius: 15 },
			br: { radius: 15 },
			antiAlias: true,
			autoPad: true
			}
			var divObj = document.getElementById("pageHeader");
			cornersObj = new curvyCorners(settings, divObj);
			cornersObj.applyCornersToAll();
			var divObj2 = document.getElementById("footerContainer");
			cornersObj = new curvyCorners(settings2, divObj2);
			cornersObj.applyCornersToAll();
			var divObj2 = document.getElementById("navOuter");
			cornersObj = new curvyCorners(settings3, divObj2);
			cornersObj.applyCornersToAll();
			var divObj2 = document.getElementById("sideTitleOuter");
			cornersObj = new curvyCorners(settings4, divObj2);
			cornersObj.applyCornersToAll();
			var divObj2 = document.getElementById("sidebarBodyOuter");
			cornersObj = new curvyCorners(settings5, divObj2);
			cornersObj.applyCornersToAll();
			}
