// 'upwards' is the global object for upwards theme functions
var upwards = {};
// Reduce potential conflicts with other scripts on the page
upwards.jQuery = jQuery.noConflict(true);
var $upwards = upwards.jQuery;
// Create a unique object and namespace for theme functions
upwards.themeFunctions = {};
// Define a closure
upwards.themeFunctions = (function() {
    // When jQuery is used it will be available as $ and jQuery but only inside the closure
    var jQuery = upwards.jQuery;
    var $ = jQuery;
	var $upwards = jQuery.noConflict();

// ExtraContent functionality 
$upwards(document).ready(function () {
	var extraContent =  (function() {
		var ecValue = 10;
		for (i=1;i<=ecValue;i++)
		{
			$upwards('#myExtraContent'+i+' script').remove();
			$upwards('#myExtraContent'+i).appendTo('#extraContainer'+i);
		}
	})();
});


$upwards(document).ready(function () {
	$upwards("#nav ul ul").css({display: "none"});
	$upwards("#nav li").hover(function(){
		$upwards(this).find('ul:first').css({visibility: "visible",display: "none"}).fadeIn(800);
		},function(){
		$upwards(this).find('ul:first').fadeOut(500);
	});
	$upwards("#nav li").each(function (i) {
		i = i+1;
		$upwards(this).addClass("link"+i);
	});
	$upwards("#nav ul").each(function (i) {
   		i = i+1;
   		$upwards(this).addClass("list"+i);
	});
});

// SS3 Slideshow functionality
$upwards(document).ready(function($){
	$upwards.SeydoggySlideshow({
		wrapper : '#ss3',
		target : '#ss3-target',
		ecValue : 4
	});
});
	
})(upwards.themeFunctions);