<script language = javascript>
var now = new Date();

//----------------------------------------------------------------------------
// • Below you can change the names of the months (for instance in another language), and the bordercolour of the table. Default is white (#FFFFFF).

var month_array = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
document.write("<form name=date_list><table bgcolor=#FFFFFF><tr><td>");
document.write("<select name=month onchange=change_month(this.options.selectedIndex)>");
for(i=0;i<month_array.length;i++)
{
if (now.getMonth() != i)
{document.write ("<option value="+i+">"+month_array[i]);}
else
{document.write ("<option value="+i+" selected>"+month_array[i]);}

}
document.write("</select>");
document.write("</td><td>");
document.write ("<select name=year onchange=change_year(this.options[this.options.selectedIndex])>");
for(i=2000;i<2100;i++)
{
if (now.getFullYear() != i)
{document.write("<option value="+i+">"+i);}
else
{document.write("<option value="+i+" selected>"+i);}
}
document.write("</select></td></tr><tr><td colspan=2><center>");

//----------------------------------------------------------------------------
// • Below you can change the backgroundcolour of the calendar (table bgcolor). Default is white (#FFFFFF).
// • Below you can change the backgroundcolour of the days (tr bgcolor). Default is white (#FFFFFF).
// • Below you can change the fontcolour of the days (font color). Default is grey (#d3d3d3).

// • The letters of the days are M, T, W, T, F, S and S, but you can change them, for instance in another language.

document.write("<table bgcolor=#FFFFFF border=0 cellspacing = 0 cellpading = 0 width=100% ><tr bgcolor=#FFFFFF align=center>");
document.write("<td><font color=#d3d3d3>M</font></td><td><font color=#d3d3d3>T</td><td><font color=#d3d3d3>W</td><td><font color=#d3d3d3>T</td><td><font color=#d3d3d3>F</td><td ><font color=#d3d3d3>S</td><td ><font color=#d3d3d3>S</td>");
document.write("</tr><tr>");
for(j=0;j<6;j++)
{
for(i=0;i<7;i++)
{
   document.write("<td align=center><div id=d"+i+"r"+j+"></div></td>")
}
document.write("</tr>");
}

document.write("</table>");

document.write("</center></from></td></tr></table>");

var show_date = new Date();

function set_cal(show_date)
{
begin_day = new Date (show_date.getFullYear(),show_date.getMonth());
begin_day_date = begin_day.getDay();
end_day = new Date (show_date.getFullYear(),show_date.getMonth()+1);
count_day = (end_day - begin_day)/1000/60/60/24;
if (begin_day.getMonth() == 2) count_day = 31;
else
if (begin_day.getMonth() == 9) count_day = 31;
input_table(begin_day_date,count_day);
}
set_cal(show_date);

function input_table(begin,count)
{
init();
j=0;
if (begin!=0){i=begin-1;}else{i=6;}
for (c=1;c<count+1;c++)
{
colum_name = "d"+i+"r"+j;
colum_name = document.getElementById(colum_name);
if ((now.getDate() == c)&&(show_date.getMonth() == now.getMonth())&&(show_date.getFullYear() == now.getFullYear()))

//----------------------------------------------------------------------------
// • Below you can change the backgroundcolour of the current day. Default is red (#FF0000).

{colum_name.style.backgroundColor = "#FF0000";colum_name.style.color = "";}
colum_name.innerHTML =  c;
i++;
if (i==7){i=0;j++;}
}
after_init();
}

function init()
{
for(j=0;j<6;j++)
{
for(i=0;i<7;i++)
{
colum_name = "d"+i+"r"+j;
colum_name = document.getElementById(colum_name);
colum_name.innerHTML =  "-";
colum_name.style.backgroundColor ="";

//----------------------------------------------------------------------------
// • Below you can change the fontcolour of the calendar (default is black #000000).

colum_name.style.color ="#000000";
}
}
}

function after_init()
{
for(j=0;j<6;j++)
{
for(i=0;i<7;i++)
{
  colum_name = "d"+i+"r"+j;
colum_name = document.getElementById(colum_name);

//----------------------------------------------------------------------------
// • Below you can change the "open days" (default is -, but you can write • or * or whatever) and the fontcolour of it (default is white #FFFFFF, so it's not visible)

if (colum_name.innerHTML == "-")
{
colum_name.style.color ="#FFFFFF";
}
}
}
}

function change_month(sel_month)
{
show_date = new Date(show_date.getFullYear(),sel_month,1);
set_cal(show_date);
}

function change_year(sel_year)
{
sel_year = sel_year.value;
show_date = new Date(sel_year,show_date.getMonth(),1);
set_cal(show_date);
}