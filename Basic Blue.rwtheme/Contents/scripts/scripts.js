// 'themeflood' is the global object for themeflood theme functions
var themeflood = {};
// Reduce potential conflicts with other scripts on the page
themeflood.jQuery = jQuery.noConflict(true);
var $themeflood = themeflood.jQuery;
// Create a unique object and namespace for theme functions
themeflood.themeFunctions = {};
// Define a closure
themeflood.themeFunctions = (function() {
    // When jQuery is used it will be available as $ and jQuery but only inside the closure
    var jQuery = themeflood.jQuery;
    var $ = jQuery;
	var $themeflood = jQuery.noConflict();
	

// ExtraContent functionality 
$themeflood(document).ready(function () {
	var extraContent =  (function() {
		var ecValue = 15;
		for (i=1;i<=ecValue;i++)
		{
			$themeflood('#myExtraContent'+i+' script').remove();
			$themeflood('#myExtraContent'+i).appendTo('#extraContainer'+i);
		}
	})();
});

// FreeStyle banners
$themeflood(document).ready(function(){
	$themeflood('#fs').appendTo('#freeStyle');                                           
});

	
})(themeflood.themeFunctions);
